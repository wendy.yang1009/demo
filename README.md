# demo
包括oc、Python、php,react-native的一些练习

### OC源码探究的例子
1. [可以直接运行的runtime源码objc4-750](https://github.com/ifgyong/demo/tree/master/OC/objc4-750)
2. [对象本质1](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day01-%E5%AF%B9%E8%B1%A1%E7%9A%84%E6%9C%AC%E8%B4%A81)
3. [对象本质2](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day01-%E5%AF%B9%E8%B1%A1%E7%9A%84%E6%9C%AC%E8%B4%A82)
4. [对象本质3](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day01-%E5%AF%B9%E8%B1%A1%E7%9A%84%E6%9C%AC%E8%B4%A83)
5. [类的本质](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day02-%E7%B1%BB%E7%9A%84%E6%9C%AC%E8%B4%A81)
6. [KVC1](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day04-KVC)
7. [KVC2](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day04-KVC2)
8. [KVC3](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day04-KVC3)
9. [KVO本质](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day03-KVO%E6%9C%AC%E8%B4%A8)
10. [Category](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day05-Category)
11. [initialize](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day06-initialize)
12. [load](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day06-load)
13. [关联对象](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day07-%E5%85%B3%E8%81%94%E5%AF%B9%E8%B1%A1) 
14. [block本质1](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day08-block)
15. [block本质2](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day08-block2)
16. [block-copy](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day08-block-copy)
17. [block 对象类型](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day09-block-%E5%AF%B9%E8%B1%A1%E7%B1%BB%E5%9E%8B)
18. [block循环引用](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day09-block5%E5%BE%AA%E7%8E%AF%E5%BC%95%E7%94%A8)
19. [runtime位运算](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day11-runtime1)
20. [runtime结构体位运算](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day11-runtime2)
21. [runtime联合体](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day11-runtime3)
22. [runtime-cache扩容清除cache](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day11-runtime1)
23. [runtime-添加缓存验证](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day11-runtime3)
24. [runtime-消息转发1](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day12-%E6%B6%88%E6%81%AF%E8%BD%AC%E5%8F%911)
25. [runtime-消息转发2](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day12-%E6%B6%88%E6%81%AF%E8%BD%AC%E5%8F%912)
26. [runtime-消息转发3](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day12-%E6%B6%88%E6%81%AF%E8%BD%AC%E5%8F%913)
27. [runtime-isKindOfClass&isMemberOfClass](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day13-class)
28. [runtime-super 探究](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day13-super)
29. [runtime-JsonToModel](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day13-super2)
30. [runitme-数组字典nil处理和btn日志记录](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-%E6%96%B9%E6%B3%95%E4%BA%A4%E6%8D%A2)
31. [设计模式demo](https://github.com/ifgyong/demo/tree/master/OC/test/test/aof)
32. [CF源码](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-runloop%E6%BA%90%E7%A0%81)
33. [thread保活c语言版本](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-CFRunloop%E7%BA%BF%E7%A8%8B%E4%BF%9D%E6%B4%BB%E5%B0%81%E8%A3%85%E7%9A%84c%E8%AF%AD%E8%A8%80)
34. [thread 保活](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-CFRunloop%E7%BA%BF%E7%A8%8B%E4%BF%9D%E6%B4%BB%E5%B0%81%E8%A3%85)
35. [runloop 多中模式切换和状态监听](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-CFRunloop1)
36. [runloop 保活](https://github.com/ifgyong/demo/tree/master/OC/OC%E6%9C%AC%E8%B4%A8/day14-CFRunloop1)
37. 

